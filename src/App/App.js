import React, { useState,useEffect } from 'react';
import { Switch, Route,Redirect } from 'react-router-dom';
import Cookies from 'universal-cookie';
import i18n from 'i18next';
import { Layout } from '../components/Layout';
import Home from '../pages/Home';
import Posts from '../pages/Posts';
import Post from '../pages/Post';
import _404 from '../pages/404';
import "bootstrap";
import "../assets/css/main.scss";
import "../assets/css/theme.scss";
import "../assets/css/responsive.scss";

const cookies = new Cookies();
export default (props)=> {
  const login = true || cookies.get('username');
  //const login = cookies.get('username');

  useEffect(()=>{
    i18n.changeLanguage(
      props.global.Idioma ? props.global.Idioma.toLowerCase() : 'es'
    );
  },[props.global])
    return (
      <Layout>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/posts/:idUser' component={Posts} />
          <Route exact path='/posts' render={()=><Posts all={true}/>} />
          <Route path='/post/:idPost' component={Post} />
          <Route  path='' component={_404} />
        </Switch>
      </Layout>
    );
}
