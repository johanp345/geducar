import {
  setIdioma,
  setGlobalConfig
} from '.';

export const getGlobalConfig = (config) => async (dispatch) => {
  try {
      dispatch(setGlobalConfig(config));
  } catch (error) {
      console.log(error)
  }
};

export const getIdioma = (idioma) => async (dispatch) => {
  try {
      dispatch(setIdioma(idioma));
  } catch (error) {
      console.log(error)
  }
};
