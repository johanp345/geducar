import {
  SET_GLOBAL,
  SET_IDIOMA_VALUE,
} from "./types";

export const setGlobalConfig = (config) => ({
  type: SET_GLOBAL,
  payload: config,
});

export const setIdioma = (value) => ({
  type: SET_IDIOMA_VALUE,
  payload: value,
});


