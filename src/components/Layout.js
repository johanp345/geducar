import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import logoGed from '../assets/images/geducar-logo.png';

export const Layout = (props) => {
  const {children}= props 
    return (
        <div className='layout w-100 d-flex flex-column'>
          <header className="py-3 w-100" id="headerPpal">
          <div className="container d-flex justify-content-between align-items-center">
            <figure className="mb-0">
              <img src={logoGed} alt="Logo ged" className="logoBlog"/>
              <figcaption className="text-center">Un blog para educar</figcaption>
            </figure>
            <ul>
              <li><Link to="/">Usuarios</Link></li>
              <li><Link to="/posts">Publicaciones</Link></li>
            </ul>
          </div>
          </header>
          <div className="container my-4">
            {children}
          </div>

          <footer className="mt-auto w-100">
          <div className="container">
            <a onClick={(e)=>{e.preventDefault()}}><span>Desarrollado por: </span> <b>Johan Páez</b></a>
          </div>
          </footer>
        </div>
    );
}
