import React, { useState,useEffect } from 'react';
import { withTranslation } from 'react-i18next';

//const cookies = new Cookies();
const _404 = (props)=> {
  const {global,t,getIdioma} = props;
  useEffect(()=>{
    console.log(props);
    setTimeout(() => {
      getIdioma("en")
    }, 5000);
  },[])
  useEffect(()=>{
    console.log(global);
  },[global])
    return (
      <div id="wrap404">
        <p>{t('titulo404')}</p>
        <span>:(</span>
      </div>
    );
}

export default withTranslation()(_404)