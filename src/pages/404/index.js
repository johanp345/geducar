import { connect } from "react-redux";
import _404 from "./404";

import { getGlobalConfig,getIdioma} from "../../actions/global/creators";

const mapStateToProps = (state) => ({
  global: state.global,
});

const mapDispatchToProps = (dispatch) => ({
  getGlobalConfig: (domain) => dispatch(getGlobalConfig(domain)),
  getIdioma:(lang)=>dispatch(getIdioma(lang))
});

export default connect(mapStateToProps, mapDispatchToProps)(_404);
