import React, { useState, useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import { Link,useHistory } from 'react-router-dom';
import axiosApi from '../../services/services';
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import userPlaceholder from '../../assets/images/user.jpg';
//const cookies = new Cookies();
const Home = (props) => {
  const { global, t,getGlobalConfig } = props;
  const [Loading, setLoading] = useState(true);
  const history=useHistory();
  const [Usuarios, setUsuarios] = useState([]);
  useEffect(() => {
    (async () => {
      const { status, data } = await axiosApi.getUsers();
      if (status === 200) {
        console.log(data);
        setUsuarios(data);
      }
      setLoading(false)
    })()
  }, [])

  const setGlobalUser = (user)=>{
    getGlobalConfig({user:user});
    history.push(`/posts/${user.id}`);
  }

  return (
    <section id="wrapUsers">
      <h3 className="title">Usuarios que han compartido contenido</h3>
      <div className="row">
        {
          Loading ?
            [1,2,3,4,5,6,7].map(s=>
              <div key={s} className="col-12 col-sm-6 col-md-4 col-lg-3">
                <div className="subComponent">
                  <SkeletonTheme color="#f6f6f6" highlightColor="#fefefe">
                    <Skeleton duration={2} height={"400px"} style={{ display: "block", marginBottom: "40px" }} />
                  </SkeletonTheme>
                </div>
              </div>
            )
            :
            Usuarios.map(u =>
              <div key={u.id} className="col-12 col-sm-6 col-md-4 col-lg-3">
                <a href="#" onClick={e=>{e.preventDefault();setGlobalUser(u)}}>
                  <div className="item-user">
                    <figure>
                      <img src={u.img ? u.img : userPlaceholder} alt="user image" />
                    </figure>
                    <p className="name">{u.name}</p>
                    <p className="username">{u.username}</p>
                  </div>
                </a>
              </div>
            )
        }
      </div>
    </section>
  );
}

export default withTranslation()(Home)