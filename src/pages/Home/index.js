import { connect } from "react-redux";
import Home from "./Home";

import { getGlobalConfig} from "../../actions/global/creators";

const mapStateToProps = (state) => ({
  global: state.global,
});

const mapDispatchToProps = (dispatch) => ({
  getGlobalConfig: (domain) => dispatch(getGlobalConfig(domain)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
