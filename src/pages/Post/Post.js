import React, { useState, useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import { Link, useParams,useHistory } from 'react-router-dom';
import axiosApi from '../../services/services';
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import userPlaceholder from '../../assets/images/user.jpg';
//const cookies = new Cookies();
const Post = (props) => {
  const { global, t } = props;
  const { idPost } = useParams();
  const history=useHistory();
  const [Loading, setLoading] = useState(true);
  const [LoadingCom, setLoadingCom] = useState(true);
  const [Post, setPost] = useState(null);
  const [ShowCom, setShowCom] = useState(false);
  const [Comments, setComments] = useState([]);
  useEffect(() => {
    if(!idPost){
      history.replace("/")
    }
    (async () => {
      let { status, data } = await axiosApi.getPostById(idPost);
      if (status === 200) {
        console.log(data);
        setPost(data);
        let response = await axiosApi.getCommentsByPost(idPost);
        if(response.status==200){
          console.log(response.data);
          setComments(response.data);
          setLoadingCom(false)
        }
      }
      setLoading(false)
    })()
  }, [])

  const toggleComments=()=>{
    setShowCom(!ShowCom);
  }

  return (
    <section id="wrapPost">
      {global.user&&
        <div className="user d-flex">
          <figure>
            <img src={global.userimg ? global.userimg : userPlaceholder} alt="user image" />
          </figure>
          <div>
            <p>{global.user.username}</p>
            <p>{global.user.email}</p>
          </div>
        </div>
      }
      <div className="row post">
        {
          Loading ?
              <div className="col-12">
                <div className="subComponent">
                  <SkeletonTheme color="#f6f6f6" highlightColor="#fefefe">
                    <Skeleton duration={2} height={"400px"} style={{ display: "block", marginBottom: "40px" }} />
                  </SkeletonTheme>
                </div>
              </div>
            :(Post)&&
              <div className="col-12">
                  <div className="item-post">
                    <p className="title">{Post.title}</p>
                    <p className="body">{Post.body}</p>
                    <a href="" onClick={(e)=>{e.preventDefault();toggleComments()}}>{ShowCom?"Ocultar comentarios":`Ver comentarios (${Comments.length})`}</a>
                    {
                      ShowCom&&
                      <section className="comments">
                        {
                          Comments.map(c=>
                            <div key={c.id} className="comment">
                              <div className="userCom">
                                <div className="name">{c.name}</div>
                                <div className="email">{c.email}</div>
                              </div>
                              <div className="body">{c.body}</div>
                            </div>
                          )
                        }
                      </section>
                    }
                  </div>
              </div>
        }
      </div>
    </section>
  );
}

export default withTranslation()(Post)