import React, { useState, useEffect } from 'react';
import { withTranslation } from 'react-i18next';
import { Link, useParams,useHistory } from 'react-router-dom';
import axiosApi from '../../services/services';
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import userPlaceholder from '../../assets/images/user.jpg';
//const cookies = new Cookies();
const Posts = (props) => {
  const { global, t,getGlobalConfig,all } = props;
  const { idUser } = useParams();
  const history=useHistory();
  const [Loading, setLoading] = useState(true);
  const [Posts, setPosts] = useState([]);
  useEffect(() => {
    if(idUser&&!global.user){history.replace("/")}
    if(all){getGlobalConfig({user:null})}
    (async () => {
      const { status, data } = !all ? await axiosApi.getPostByUser(idUser) : await axiosApi.getPosts();
      if (status === 200) {
        setPosts(data);
      }
      setLoading(false)
    })()
  }, [props.all])

  return (
    <section id="wrapPosts">
      {global.user&&idUser &&
        <div className="user d-flex">
          <figure>
            <img src={global.userimg ? global.userimg : userPlaceholder} alt="user image" />
          </figure>
          <div>
            <p>{global.user.username}</p>
            <p>{global.user.email}</p>
          </div>
        </div>
      }
      <div className="row listPosts">
        {
          Loading ?
            [1, 2, 3, 4, 5, 6, 7].map(s =>
              <div key={s} className="col-12">
                <div className="subComponent">
                  <SkeletonTheme color="#f6f6f6" highlightColor="#fefefe">
                    <Skeleton duration={2} height={"100px"} style={{ display: "block", marginBottom: "40px" }} />
                  </SkeletonTheme>
                </div>
              </div>
            )
            :
            Posts.map(p =>
              <div key={p.id} className="col-12 col-lg-6 align-items-stretch d-flex">
                  <div className="item-post">
                    <p className="title">{p.title}</p>
                    <p className="body">{p.body}</p>
                    <Link to={`/post/${p.id}`}>Ver más</Link>
                  </div>
              </div>
            )
        }
      </div>
    </section>
  );
}

export default withTranslation()(Posts)