import {
  SET_GLOBAL,
  SET_IDIOMA_VALUE,
} from '../actions/global/types';

const initialState = {
  isLoaded: 0,
  Idioma: 'es',
};

const globalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_GLOBAL: {
      return {
        ...state,
        ...action.payload,
      };
    }
    case SET_IDIOMA_VALUE: {
      return {
        ...state,
        Idioma: action.payload,
      };
    }
    default:
      return state;
  }
};

export default globalReducer;
