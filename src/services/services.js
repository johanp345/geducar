import axios from 'axios';
const axiosApi=axios.create({
    baseURL:"https://jsonplaceholder.typicode.com/",
    mode: 'cors'
})

export default {
    getUsers:()=>
        axiosApi.get("users").catch(err=>err),
    getPosts:()=>
        axiosApi.get(`posts`).catch(err=>err),
    getPostByUser:(UserId)=>
        axiosApi.get(`users/${UserId}/posts`).catch(err=>err),
    getPostById:(id)=>
        axiosApi.get(`posts/${id}`).catch(err=>err),
    getCommentsByPost:(PostId)=>
        axiosApi.get(`posts/${PostId}/comments`).catch(err=>err),
}